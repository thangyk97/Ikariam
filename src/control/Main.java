package control;

import java.awt.EventQueue;

import model.FileIsland;
import view.IslandView;

public class Main {
  public static void main(String[] args) {
    
    FileIsland.saveFile(null); // reset text file save island object
    
    EventQueue.invokeLater(new Runnable() {
      
      @Override
      public void run() {
        
        try {
          
          IslandView islandFrame = new IslandView();
          
          islandFrame.setVisible(true);
          
        } catch(Exception e) {
          e.printStackTrace();
        }
        
      }
    });
  }
}
