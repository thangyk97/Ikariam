package model.army.unitHasMunition;

import model.army.Unit;

public class Catapult extends UnitHasMunition {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public Catapult(int hitPoint, int armour, int size, int rankArmour, int rankDamage, int accuracy, int speed,
      int damage, int munition) {
    super(hitPoint, armour, size, rankArmour, rankDamage, accuracy, speed, damage, munition);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void attack(Unit unit) {
    
    if (rand.nextInt(100) + 1 > accuracy) {
      return;
    }
    
    if (unit.getArmour() - damage < 0) {
      unit.setHitPoint(unit.getHitPoint() + unit.getArmour() - damage);
    }    
  }

  @Override
  public String getImg() {
    // TODO Auto-generated method stub
    return "/img/Captapult.gif";
  }

}
