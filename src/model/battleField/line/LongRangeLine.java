package model.battleField.line;

import model.army.Army;
import model.army.inforOfUnit;
import model.battleField.Cell;

public class LongRangeLine extends Line {

  public LongRangeLine(int theNumberOfCell, int sizeOfCell, int sizeOfArray, Army army) {
    super(theNumberOfCell, sizeOfCell, sizeOfArray, army);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void sort(Army army) {
    
    int n = theNumberOfCell;
    
    int j = 7 / 2;

    for (int i = 0; i < n; i++) {
      
      if (i % 2 == 0) {
        i = - i;
      }
      
      j = j - i;
      // only change bellow
      
      // sulphur full of cell
      if (army.getGuns().size() 
          / (sizeOfCell / inforOfUnit.SULPHUR_CARABINEER[2]) > 0) {
        
        cells[j] = new Cell();
        
        for (int x = 0; x < sizeOfCell / inforOfUnit.SULPHUR_CARABINEER[2]; x++) {
          
          cells[j].getUnits().add(army.getGuns().remove(0));
          
        }
        
        // sulphur and archer not full of cell ==> add sulphur
      } else if (! army.getGuns().isEmpty() 
          && army.getArchers().size() 
            / (sizeOfCell / inforOfUnit.ARCHER[2]) < 1) {
        
        cells[j] = new Cell();
        
        for (int x = 0; x < army.getGuns().size(); x++) {
          
          cells[j].getUnits().add(army.getGuns().remove(0));
          
        }

        // sulphur not full of cell and archer full of cell
      } else if (army.getArchers().size()
          / (sizeOfCell / inforOfUnit.ARCHER[2]) > 0) {
        
        cells[j] = new Cell();
        
        for (int x = 0; x < sizeOfCell / inforOfUnit.ARCHER[2]; x++) {
          
          cells[j].getUnits().add(army.getArchers().remove(0));
          
        }
        
        // sulphur empty and archer not full of cell
      } else if (! army.getArchers().isEmpty()) {
        
        cells[j] = new Cell();
        
        for (int x = 0; x < army.getArchers().size(); x++) {
          
          cells[j].getUnits().add(army.getArchers().remove(0));
          
        }
      }
      // only change above
      i = Math.abs(i);
    }    
  }

}
