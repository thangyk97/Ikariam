package model.battleField.line;

import model.army.Army;
import model.army.inforOfUnit;
import model.battleField.Cell;

public class BomberLine extends Line {

  public BomberLine(int theNumberOfCell, int sizeOfCell, int sizeOfArray, Army army) {
    super(theNumberOfCell, sizeOfCell, sizeOfArray, army);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void sort(Army army) {
    
    for (int i = 0; i < theNumberOfCell; i++) {
      
      if (army.getBombers().size() 
          / (sizeOfCell / inforOfUnit.BOOMBARDIER[2]) > 0) {
        
        cells[i] = new Cell();
        
        for (int x = 0; x < sizeOfCell / inforOfUnit.BOOMBARDIER[2]; x++) {
          
          cells[i].getUnits().add(army.getBombers().remove(0));
          
        }
        
      } else if (army.getBombers().size()
          % (sizeOfCell / inforOfUnit.BOOMBARDIER[2]) > 0) {
        
        cells[i] = new Cell();
        
        for (int x = 0; x < army.getBombers().size(); x++) {
          
          cells[i].getUnits().add(army.getBombers().remove(0));
          
        }
      }
    }    
  }

}
