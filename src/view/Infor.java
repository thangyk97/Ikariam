package view;

public class Infor {
  public static final String[] NAME = {
      "Hoplite",
      "Steam Giant",
      "Sulphur Carabineer",
      "Archer",
      "Slinger",
      "Mortar",
      "Catapult",
      "Ram",
      "Boombardier",
      "Gyrocoper",
      "Spearman",
      "Swordsman",
      "Cook",
      "Doctor"
  };
  public static final String[] LINK = {
      "/img/Hoplite.gif",
      "/img/Steam_Giant.gif",
      "/img/Marksmen.gif",
      "/img/Archer.gif",
      "/img/Slinger.gif",
      "/img/Mortar.gif",
      "/img/Captapult.gif",
      "/img/Ram.gif",
      "/img/Bombardier.gif",
      "/img/Gyrocopter.gif",
      "/img/Spearman.png",
      "/img/Swordsman.gif",
      "/img/Cook.gif",
      "/img/Doctor.gif"
  };
}
